# 1. WebDev_01 ()
HTML Struktur

En forudsætning for at kunne udvikle platformsuafhængige mobilapplikationer er et solidt kendskab til HTML elementer og dokumenternes struktur.

# 2. WebDev_02 ()
CSS

CSS er malingen. Når strukturen er på plads, applikerer vi CSS regler i overensstemmelse med designerens retningslinjer.

# 3. MobDev_01 ()
Ionic

Ionic hjælper os både med elementer og stylingen af vores brugergrænseflade. Med Ionic får vi en masse materiale stillet til rådighed. Dette kan medvirke til en hurtigere udviklingstid, samt en standardiseret og velafprøvet implementering.

# 4. MobDev_02 ()
JavaScript

JavaScript håndterer adfærden, logikken og transaktionen af data imellem klient og server. JavaScript er en forudsætning og kræves før vi kan begynde at arbejde med JavaScript biblioketer såsom AngularJS

# 5. MobDev_03 ()
AngularJS

Når vi er blevet bekendt med JavaScript inkorporerer vi Angularjs. Biblioteket er særligt behjælpelig i forbindelse med udvikle af kompliceret web applikationer.

# 6. MobDev_04 ()
Cordova PhoneGap

Vi anvender Phonegap til at pakke vores applikation ind i en platformspecifik ramme.

# 8. MobDev_05 ()
Eget projekt

Med udgangspunkt i introduceret teknologier og sprog, vælger i nu selv et projekt, som i ønsker at arbejde med.